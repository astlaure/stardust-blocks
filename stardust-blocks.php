<?php
/*
Plugin Name: Stardust Blocks
Description: Blocks registered by Stardust
Version: 1.0.0
*/

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('StardustBlocks')) {
    class StardustBlocks
    {
        function __construct()
        {
            add_action('init', array($this, 'register_scripts'));
            add_action('enqueue_block_assets', array($this, 'enqueue_block_assets'));
            add_action('enqueue_block_editor_assets', array($this, 'enqueue_block_editor_assets'));
        }

        function register_scripts()
        {
            // automatically load dependencies and version
            $asset_file = include(plugin_dir_path(__FILE__) . 'build/index.asset.php');

            wp_register_script(
                'stardust-blocks-gutenberg',
                plugins_url('build/index.js', __FILE__),
                $asset_file['dependencies'],
                $asset_file['version']
            );

            $this->register_block('stardust/button');

            $this->register_block('stardust/facebook-social');
			$this->register_block('stardust/github-social');
			$this->register_block('stardust/instagram-social');
			$this->register_block('stardust/youtube-social');

            $this->register_block_style('core/button', 'primary', 'Primary Button');
            $this->register_block_style('core/button', 'secondary', 'Secondary Button');
        }

        function enqueue_block_assets()
        {
            wp_enqueue_style(
                'stardust-blocks-style',
                plugins_url('assets/css/style.css', __FILE__),
                array(),
                '1.0.0'
            );
        }

        function enqueue_block_editor_assets()
        {
            wp_enqueue_style(
                'stardust-blocks-editor',
                plugins_url('assets/css/editor.css', __FILE__),
                array(),
                '1.0.0'
            );
        }

        function register_block($name)
        {
            register_block_type($name, array(
                'api_version' => 2,
                'editor_script' => 'stardust-blocks-gutenberg',
                // 'style' => 'stardust-blocks'
            ));
        }

        function register_dynamic_block($name, $callback)
        {
            register_block_type($name, array(
                'api_version' => 2,
                'editor_script' => 'stardust-blocks-gutenberg',
                'render_callback' => array($this, $callback)
            ));
        }

        function register_block_style($block, $name, $label)
        {
            register_block_style($block, array(
                'name' => $name,
                'label' => __($label, 'stardust-blocks')
            ));
        }

        static public function activate() {
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

            if ( !is_plugin_active( 'stardust-core/stardust-core.php' ) ) {
                ?>
                <div class="notice notice-error" >
                    <p>Please enable Stardust Core before activating Stardust I18n</p>
                </div>
                <?php
                @trigger_error(__( 'Please enable Stardust Core before activating Stardust I18n', 'stardust-blocks' ), E_USER_ERROR);
            }

            update_option( 'rewrite_rules', '' );
        }

        static public function deactivate() {
            flush_rewrite_rules();
        }

        static public function uninstall() {}
    }
}

if (class_exists('StardustBlocks')) {
    register_activation_hook( __FILE__, array( 'StardustBlocks', 'activate' ) );
    register_deactivation_hook( __FILE__, array( 'StardustBlocks', 'deactivate' ) );
    register_uninstall_hook( __FILE__, array( 'StardustBlocks', 'uninstall' ) );

    new StardustBlocks();
}
