import { registerBlockType } from '@wordpress/blocks';
import { FacebookSocialEdit } from './edit';
import { FacebookIcon } from './icon';
import { FacebookSocialSave } from './save';
 
registerBlockType( 'stardust/facebook-social', {
    apiVersion: 2,
    title: 'Facebook Social',
    icon: FacebookIcon,
    category: 'widgets',
    example: {},
    attributes: {
        href: {
            type: 'string'
        }
    },
    edit: FacebookSocialEdit,
    save: FacebookSocialSave,
} );
