import { registerBlockType } from '@wordpress/blocks';
import { InstagramSocialEdit } from './edit';
import { InstagramIcon } from './icon';
import { InstagramSocialSave } from './save';

registerBlockType( 'stardust/instagram-social', {
  apiVersion: 2,
  title: 'Instagram Social',
  icon: InstagramIcon,
  category: 'widgets',
  example: {},
  attributes: {
    href: {
      type: 'string'
    }
  },
  edit: InstagramSocialEdit,
  save: InstagramSocialSave,
} );
