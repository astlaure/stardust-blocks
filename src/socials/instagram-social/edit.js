import { useBlockProps, InspectorControls } from '@wordpress/block-editor';
import { PanelBody, TextControl } from '@wordpress/components';
import { InstagramIcon } from './icon';

export const InstagramSocialEdit = (props) => {
  const { attributes, setAttributes } = props;
  const blockProps = useBlockProps();

  return (
    <>
      <InspectorControls>
        <PanelBody initialOpen={true} title='Social url' opened={true}>
          <TextControl value={attributes.href} onChange={(value) => setAttributes({ href: value })} />
        </PanelBody>
      </InspectorControls>
      <a {...blockProps} target="_blank">
        <InstagramIcon />
      </a>
    </>
  );
}
