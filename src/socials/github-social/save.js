import { useBlockProps } from '@wordpress/block-editor';
import { GithubIcon } from './icon';
import classNames from 'classnames';

export const GithubSocialSave = (props) => {
  const { attributes } = props;
  const blockProps = useBlockProps.save();

  const classes = classNames('stardust-social', blockProps.className);

  return (
    <a {...blockProps} className={classes} href={attributes.href} target="_blank">
      <GithubIcon />
    </a>
  );
}
