import { registerBlockType } from '@wordpress/blocks';
import { GithubSocialEdit } from './edit';
import { GithubIcon } from './icon';
import { GithubSocialSave } from './save';

registerBlockType( 'stardust/github-social', {
  apiVersion: 2,
  title: 'Github Social',
  icon: GithubIcon,
  category: 'widgets',
  example: {},
  attributes: {
    href: {
      type: 'string'
    }
  },
  edit: GithubSocialEdit,
  save: GithubSocialSave,
} );
