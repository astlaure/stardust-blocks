import { registerBlockType } from '@wordpress/blocks';
import { YoutubeSocialEdit } from './edit';
import { YoutubeIcon } from './icon';
import { YoutubeSocialSave } from './save';

registerBlockType( 'stardust/youtube-social', {
  apiVersion: 2,
  title: 'Youtube Social',
  icon: YoutubeIcon,
  category: 'widgets',
  example: {},
  attributes: {
    href: {
      type: 'string'
    }
  },
  edit: YoutubeSocialEdit,
  save: YoutubeSocialSave,
} );
