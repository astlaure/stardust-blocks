import { registerBlockType } from '@wordpress/blocks';
import { ButtonEdit } from './edit';
import { ButtonSave } from './save';
 
registerBlockType( 'stardust/button', {
    apiVersion: 2,
    title: 'Stardust Button',
    icon: 'universal-access-alt',
    category: 'design',
    example: {},
    edit: ButtonEdit,
    save: ButtonSave,
} );