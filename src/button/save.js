import { useBlockProps } from '@wordpress/block-editor';

export const ButtonSave = () => {
    const blockProps = useBlockProps.save();

    return (
        <div { ...blockProps }>
            Hello World (from the frontend).
        </div>
    );
}