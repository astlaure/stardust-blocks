import { useBlockProps } from '@wordpress/block-editor';

export const ButtonEdit = () => {
    const blockProps = useBlockProps();

    return (
        <div { ...blockProps }>Hello World (from the editor).</div>
    );
}